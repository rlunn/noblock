const express = require('express');
const app = express();

app.get('/ping', (req, res) => {
    res.send('pong\n');
})

app.get('/block-event-loop', (req, res) => {
    for (let i = 0; i < 5000000; i++) {
        console.log(i);
    }
    res.send('Blocked event loop done');
})

app.get('/await', async (req, res) => {
    await new Promise((resolve) => setTimeout(() => resolve(1), 5000));
    res.send('Await done');
})

app.listen(4000, '0.0.0.0');

